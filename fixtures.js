const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');

const Artist = require("./models/Artist");
const Album = require("./models/Album");
const Track = require("./models/Track");
const User = require("./models/User");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [Marley, Rihanna] = await Artist.create({
        title: 'Bob Marley',
        image: 'fixtures/marley.jpg',
        description: 'Bob Marley',
        published: true
    }, {
        title: 'Rihanna',
        image: 'fixtures/rihanna.png',
        description: 'Rihanna',
        published: false
    });

    const [marleyAlbum, rihannaAlbum] = await Album.create({
        title: 'One love',
        year: 2007,
        image: 'fixtures/maral.jpg',
        artist: Marley._id,
        published: false
    }, {
        title: 'Anti',
        year: 2020,
        image: 'fixtures/ririal.jpeg',
        artist: Rihanna._id,
        published: true
    });

    await Track.create({
        title: 'Stir It Up',
        duration: 3,
        album: marleyAlbum._id,
        trackNumber: 1,
        published: false
    }, {
        title: 'Get Up, Stand Up',
        duration: 3,
        album: marleyAlbum._id,
        trackNumber: 2,
        published: true
    }, {
        title: 'I Shot the Sheriff',
        duration: 3,
        album: marleyAlbum._id,
        trackNumber: 3,
        published: true
    }, {
        title: 'No Woman, No Cry',
        duration: 3,
        album: marleyAlbum._id,
        trackNumber: 4,
        published: true
    }, {
        title: 'Jamming',
        duration: 3,
        album: marleyAlbum._id,
        trackNumber: 5,
        published: false
    }, {
        title: 'Consideration',
        duration: 2,
        album: rihannaAlbum._id,
        trackNumber: 1,
        published: true
    }, {
        title: 'James Joint',
        duration: 1,
        album: rihannaAlbum._id,
        trackNumber: 2,
        published: false
    }, {
        title: 'Kiss It Better',
        duration: 2,
        album: rihannaAlbum._id,
        trackNumber: 3,
        published: true
    }, {
        title: 'Work',
        duration: 3,
        album: rihannaAlbum._id,
        trackNumber: 4,
        published: true
    }, {
        title: 'Desperado',
        duration: 2,
        album: rihannaAlbum._id,
        trackNumber: 5,
        published: false
    });

    await User.create({
        email: 'user@test',
        password: '123',
        token: nanoid(),
        role: 'user',
        displayName: 'User'
    }, {
        email: 'admin@test',
        password: '123',
        token: nanoid(),
        role: 'admin',
        displayName: 'Admin'
    });

    await mongoose.connection.close();
};

run().catch(console.error);