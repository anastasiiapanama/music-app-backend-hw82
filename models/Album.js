const mongoose = require('mongoose');

const AlbumSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    year: {
        type: Number,
        required: true
    },
    image: String,
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist',
        required: true
    },
    published: {
        type: Boolean,
        default: false
    }
});

const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;