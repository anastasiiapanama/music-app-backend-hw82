const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    image: String,
    description: {
        type: String
    },
    published: {
        type: Boolean,
        default: false
    }
});

const Artist = mongoose.model('Artist', ArtistSchema);
module.exports = Artist;