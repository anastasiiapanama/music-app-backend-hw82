const mongoose = require('mongoose');

const TrackSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    duration: {
        type: Number,
        required: true
    },
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Album',
        required: true
    },
    trackNumber: {
        type: Number,
        required: true
    },
    image: String,
    published: {
        type: Boolean,
        default: false
    }
});

const Track = mongoose.model('Track', TrackSchema);
module.exports = Track;