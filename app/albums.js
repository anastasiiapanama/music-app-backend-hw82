const express = require('express');
const path = require('path');
const multer = require('multer');

const Album = require('../models/Album');

const {nanoid} = require('nanoid');
const router = express.Router();
const config = require('../config');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
    destination: (req, file, cb) => { // cb = callback
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const albumsList = {};

        if (req.query.artist) {
            albumsList.artist = req.query.artist;
        }

        const albums = await Album.find(albumsList).sort({year: 1}).populate('artist', 'title');

        res.send(albums);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const album = await Album.findOne({_id: req.params.id}).populate('artist', 'title');

        if(album) {
            res.send(album);
        } else {
            res.sendStatus(400);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, permit('user', 'admin'), upload.single('image'), async (req, res) => {
    try {
        const albumRequestData = req.body;
        albumRequestData.user = req.user._id;

        if (req.file) {
            albumRequestData.image = req.file.filename;
        }

        const album = new Album(albumRequestData);
        await album.save();

        res.send(album);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const reqAlbum = req.body;
        reqAlbum.user = req.user._id;

        if (reqAlbum.user) {
            const album = await Album.findOne({_id: req.params.id}).remove();

            res.send(album);
        } else {
            res.sendStatus(403);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const album = await Album.findById(req.params.id);
        album.published = true;
        await album.save();

        res.send(album);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;