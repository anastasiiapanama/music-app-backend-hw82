const express = require('express');
const User = require('../models/User');
const config = require("../config");
const axios = require("axios");
const path = require('path');
const multer = require('multer');
const {nanoid} = require("nanoid");
const { OAuth2Client } = require('google-auth-library');
const googleClient = new OAuth2Client(config.google.clientId);

const storage = multer.diskStorage({
    destination: (req, file, cb) => { // cb = callback
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', upload.single('avatarImage'), async (req, res) => {
    try {
        const userData = {
            email: req.body.email,
            password: req.body.password,
            displayName: req.body.displayName
        };

        if (req.file) {
            userData.avatarImage = 'uploads/' + req.file.filename;
        }

        const user = new User(userData);

        user.generateToken();
        await user.save();

        console.log(user)
        return res.send(user);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/sessions', async (req, res) => {
   const user = await User.findOne({email: req.body.email});

   if (!user) {
       return res.status(401).send({message: 'Credentials are wrong'});
   }

   const isMatch = await user.checkPassword(req.body.password);

   if(!isMatch) {
       return res.status(401).send({message: 'Credentials are wrong'});
   }

   user.generateToken();

   await user.save();

   return res.send({message: 'Username and password correct!', user});
});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Success'};

    if (!token) {
        return res.send(success);
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.send(success);
    }

    user.generateToken();

    await user.save();

    return res.send(success);
});

router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        if (response.data.data.error) {
            return res.status(401).send({message: 'Facebook token incorrect'});
        }

        if (response.data.data['user_id'] !== req.body.id) {
            return res.status(401).send({global: 'User ID incorrect'});
        }

        let user = await User.findOne({email: req.body.email});

        if (!user) {
            user = await User.findOne({faceBook: req.body.id});
        }

        if(!user) {
            user = new User({
                email: req.body.email || nanoid(6),
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name,
                avatarImage: req.body.picture.data.url
            });
        }

        user.generateToken();
        await user.save();

        res.send({message: 'Success', user});
    } catch (e) {
        return res.status(401).send({global: 'Facebook token incorrect'});
    }

});

router.post('/googleLogin', async (req, res) => {
    try {
        const ticket = await googleClient.verifyIdToken({
            idToken: req.body.tokenId,
            audience: process.env.CLIENT_ID
        });

        const {name, email, sub: ticketUserId} = ticket.getPayload(); // можно взять picture

        if (req.body.googleId !== ticketUserId) {
            return res.status(401).send({global: 'User ID incorrect!'});
        }

        let user = await User.findOne({email});

        if(!user) {
            user = new User({
                email: email,
                password: nanoid(),
                displayName: name
            });
        }

        user.generateToken();
        await user.save();

        return res.send({message: 'Success', user});
    } catch (e) {
        console.log(e)
        return res.status(500).send({global: 'Server error. Please try again'});
    }
});

module.exports = router;