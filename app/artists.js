const express = require('express');

const Artist = require('../models/Artist');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const artists = await Artist.find();

        res.send(artists);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, permit('user', 'admin'), async (req, res) => {
    try {
        const artistRequestData = req.body;
        artistRequestData.user = req.user._id;

        const artist = new Artist(artistRequestData);
        await artist.save();

        res.send(artist);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const reqArtist = req.body;
        reqArtist.user = req.user._id;

        if (reqArtist.user) {
            const artist = await Artist.findOne({_id: req.params.id}).remove();

            res.send(artist);
        } else {
            res.sendStatus(403);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const artist = await Artist.findById(req.params.id);
        artist.published = true;
        await artist.save();

        res.send(artist);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;