const express = require('express');
const Trackhistories = require('../models/TrackHistory');
const auth = require("../middleware/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {
    try {
        const trackHistory = new Trackhistories();
        trackHistory.user = req.user._id;
        trackHistory.track = req.body.track;

        await trackHistory.save();

        return res.send(trackHistory);
    } catch (e) {
        res.send(e);
    }
});

router.get('/', auth, async (req, res) => {
    try {
        const criteria = {};

        if (req.query.user) {
            criteria.user = req.query.user;
        }

        const trackHistory = await Trackhistories.find(criteria)
            .populate('track')
            .populate('user')
            .sort({datetime: -1})

        res.send(trackHistory);
    } catch (e) {
        res.send(e);
    }
});

module.exports = router;