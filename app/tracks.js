const express = require('express');
const Track = require('../models/Track');
const auth = require("../middleware/auth");
const path = require('path');
const multer = require('multer');
const permit = require("../middleware/permit");
const {nanoid} = require("nanoid");
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => { // cb = callback
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const tracksList = {};

        if (req.query.album) {
            tracksList.album = req.query.album;
        }

        const tracks = await Track.find(tracksList).populate('album', 'title');

        res.send(tracks);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, permit('user', 'admin'), upload.single('image'), async (req, res) => {
    try {
        const trackRequestData = req.body;
        trackRequestData.user = req.user._id;

        if (req.file) {
            trackRequestData.image = req.file.filename;
        }

        const track = new Track(trackRequestData);
        await track.save();

        res.send(track);
    } catch (e) {
        res.send(e);
    }
});

router.post('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const track = await Track.findById(req.params.id);
        track.published = true;
        await track.save();

        res.send(track);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;