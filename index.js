const express = require('express');
const cors = require('cors');

const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');

const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const users = require('./app/users');
const trackhistories = require('./app/trackhistories');

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8001;

app.use('/artists', artists);
app.use('/albums', albums);
app.use('/tracks', tracks);
app.use('/users', users);
app.use('/track_history', trackhistories);

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async callback => {
        await mongoose.disconnect();
        console.log('mongoose disconnected');
        callback();
    });
};

run().catch(console.error);